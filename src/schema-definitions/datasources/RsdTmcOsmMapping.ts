export const RsdTmcOsmMappingTable: any = {
    name: "RsdTmcOsmMapping",
    pgSchema: "traffic",
    pgTableName: "rsd_tmc_osm_mapping",
};

export interface IRsdTmcOsmMapping {
    lt_start: number;
    lt_end: number;
    osm_path: string;
}
