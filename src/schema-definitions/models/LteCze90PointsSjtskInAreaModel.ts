import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ILteCze90PointsSjtskInArea } from "./interfaces/ILteCze90PointsSjtskInArea";

export class LteCze90PointsSjtskInAreaModel extends Model<ILteCze90PointsSjtskInArea> implements ILteCze90PointsSjtskInArea {
    declare cid: number;
    declare tabcd: number;
    declare lcd: number;
    declare class: string;
    declare tcd: number;
    declare stcd: number;
    declare jnumber: string;
    declare roadnumber: string;
    declare roadname: string;
    declare firstname: string;
    declare secondname: string;
    declare area_ref: number;
    declare area_name: string;
    declare roa_lcd: number;
    declare seg_lcd: number;
    declare roa_type: string;
    declare inpos: number;
    declare outpos: number;
    declare inneg: number;
    declare outneg: number;
    declare presentpos: number;
    declare presentneg: number;
    declare interrupt: number;
    declare urban: number;
    declare int_lcd: string;
    declare neg_off: number;
    declare pos_off: number;
    declare wgs84_x: string;
    declare wgs84_y: string;
    declare sjtsk_x: string;
    declare sjtsk_y: string;
    declare isolated: number;
    declare inside_project_area: string;

    public static attributeModel: ModelAttributes<LteCze90PointsSjtskInAreaModel> = {
        cid: DataTypes.INTEGER,
        tabcd: DataTypes.INTEGER,
        lcd: { type: DataTypes.INTEGER, primaryKey: true },
        class: DataTypes.CHAR,
        tcd: DataTypes.INTEGER,
        stcd: DataTypes.INTEGER,
        jnumber: DataTypes.STRING,
        roadnumber: DataTypes.STRING,
        roadname: DataTypes.STRING,
        firstname: DataTypes.STRING,
        secondname: DataTypes.STRING,
        area_ref: DataTypes.INTEGER,
        area_name: DataTypes.STRING,
        roa_lcd: DataTypes.INTEGER,
        seg_lcd: DataTypes.INTEGER,
        roa_type: DataTypes.STRING,
        inpos: DataTypes.INTEGER,
        outpos: DataTypes.INTEGER,
        inneg: DataTypes.INTEGER,
        outneg: DataTypes.INTEGER,
        presentpos: DataTypes.INTEGER,
        presentneg: DataTypes.INTEGER,
        interrupt: DataTypes.INTEGER,
        urban: DataTypes.INTEGER,
        int_lcd: DataTypes.STRING,
        neg_off: DataTypes.INTEGER,
        pos_off: DataTypes.INTEGER,
        wgs84_x: DataTypes.STRING,
        wgs84_y: DataTypes.STRING,
        sjtsk_x: DataTypes.STRING,
        sjtsk_y: DataTypes.STRING,
        isolated: DataTypes.INTEGER,
        inside_project_area: DataTypes.STRING,
    };
}
