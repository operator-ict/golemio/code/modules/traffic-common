import { IRsdTmcOsmMapping } from "#sch";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class RsdTmcOsmMappingModel extends Model<IRsdTmcOsmMapping> implements IRsdTmcOsmMapping {
    declare lt_start: number;
    declare lt_end: number;
    declare osm_path: string;

    public static attributeModel: ModelAttributes<RsdTmcOsmMappingModel> = {
        lt_start: {
            type: DataTypes.INTEGER,
        },
        lt_end: {
            type: DataTypes.INTEGER,
        },
        osm_path: {
            type: DataTypes.STRING,
        },
    };

    public static jsonSchema: JSONSchemaType<IRsdTmcOsmMapping[]> = {
        type: "array",
        items: {
            type: "object",
            required: ["lt_start", "lt_end", "osm_path"],
            additionalProperties: false,
            properties: {
                lt_start: { type: "number" },
                lt_end: { type: "number" },
                osm_path: { type: "string" },
            },
        },
    };
}
