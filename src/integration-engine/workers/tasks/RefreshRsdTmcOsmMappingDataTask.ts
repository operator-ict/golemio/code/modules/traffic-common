import { RsdTmcOsmMappingRepository } from "#ie/repositories/RsdTmcOsmMappingRepository";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Readable } from "stream";

export class RefreshRsdTmcOsmMappingDataTask extends AbstractEmptyTask {
    public readonly queueName = "RsdTmsOsmMappingRefresh";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes

    constructor(
        queuePrefix: string,
        private streamedStrategy: HTTPRequestProtocolStrategyStreamed,
        private repository: RsdTmcOsmMappingRepository
    ) {
        super(queuePrefix);
    }

    protected async execute(): Promise<void> {
        let dataStream: { data: Readable };

        try {
            dataStream = await this.streamedStrategy.getRawData();
        } catch (err) {
            throw new GeneralError("Error while getting data stream.", this.constructor.name, err);
        }

        try {
            await this.repository.replaceWithStreamData(dataStream.data);
        } catch (err) {
            throw new GeneralError("Error while replacing old data with new data from stream.", this.constructor.name, err);
        }
    }
}
