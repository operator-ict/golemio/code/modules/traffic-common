import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { RefreshRsdTmcOsmMappingDataTask } from "./tasks/RefreshRsdTmcOsmMappingDataTask";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { TrafficCommonContainer } from "#ie/ioc/Di";

export class RsdTmcOsmMappingWorker extends AbstractWorker {
    protected name = "RsdTmcOsmMapping";

    constructor() {
        super();
        this.registerTask(
            new RefreshRsdTmcOsmMappingDataTask(
                this.getQueuePrefix(),
                TrafficCommonContainer.resolve(ModuleContainerToken.RsdTmcOsmMappingDataSourceStrategy),
                TrafficCommonContainer.resolve(ModuleContainerToken.RsdTmcOsmMappingRepository)
            )
        );
    }
}
