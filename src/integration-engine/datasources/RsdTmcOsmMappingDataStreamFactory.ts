import { IProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/IProtocolStrategy";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RsdTmcOsmMappingDataStreamFactory {
    public getProtocolStrategyStreamed(dataUrl: string): IProtocolStrategy {
        return new HTTPRequestProtocolStrategyStreamed({
            headers: {},
            method: "GET",
            url: dataUrl,
        });
    }
}
