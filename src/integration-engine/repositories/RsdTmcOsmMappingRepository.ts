import { RsdTmcOsmMappingTable } from "#sch";
import { RsdTmcOsmMappingModel } from "#sch/models/RsdTmcOsmMappingModel";
import { ILogger } from "@golemio/core/dist/helpers";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { from as copyFrom } from "pg-copy-streams";
import { Readable } from "stream";

export interface LocationConfigOptions {
    ltStart: number;
    ltEnd: number;
}

@injectable()
export class RsdTmcOsmMappingRepository extends PostgresModel implements IModel {
    public constructor(
        @inject(ContainerToken.PostgresConnector) private connector: IPostgresConnector,
        @inject(ContainerToken.Logger) private logger: ILogger
    ) {
        super(
            "RsdTmcOsmMappingRepository",
            {
                outputSequelizeAttributes: RsdTmcOsmMappingModel.attributeModel,
                pgTableName: RsdTmcOsmMappingTable.pgTableName,
                pgSchema: RsdTmcOsmMappingTable.pgSchema,
                savingType: "insertOnly",
                attributesToRemove: ["id"],
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator("RsdTmcOsmMappingValidator", RsdTmcOsmMappingModel.jsonSchema)
        );
    }

    public async cleanTable(): Promise<void> {
        await this.sequelizeModel.destroy({
            truncate: true,
        });
    }

    /**
     * Replace all current data with data from a given input stream
     *
     * @param data The input stream to insert from
     */
    public async replaceWithStreamData(data: Readable): Promise<void> {
        const connection = this.connector.getConnection();
        const res = await connection.query<{ tablenametmp?: string }>(
            `CALL ${RsdTmcOsmMappingTable.pgSchema}.create_tmp_table($1, null)`,
            {
                bind: [this.tableName],
                plain: true,
                type: QueryTypes.SELECT,
            }
        );
        if (typeof res?.tablenametmp === "undefined") {
            throw new TypeError(`Got undefined tablenametmp from call of create_tmp_table for table ${this.tableName}.`);
        }
        await this.streamDataInTable(data, res.tablenametmp);
        await connection.query(`CALL ${RsdTmcOsmMappingTable.pgSchema}.replace_with_tmp_table($1)`, { bind: [this.tableName] });
    }

    /**
     * Insert data from a given input stream
     *
     * @param data The input stream to insert from
     */
    public async streamDataIn(data: Readable): Promise<void> {
        return this.streamDataInTable(data, RsdTmcOsmMappingTable.pgTableName);
    }

    /**
     * Insert data from a given input stream to a given table
     *
     * @param data The input stream to insert from
     * @param tableName The name of the table to insert to
     */
    private async streamDataInTable(data: Readable, tableName: string): Promise<void> {
        const connection = this.connector.getConnection();
        const client: any = await connection.connectionManager.getConnection({ type: "write" });

        // copy transformed data to tmp table by stream
        await new Promise<void>((resolve, reject) => {
            const stream = client
                .query(
                    copyFrom(
                        `COPY "${RsdTmcOsmMappingTable.pgSchema}".${tableName}
                        FROM STDIN DELIMITER ',' CSV HEADER;`
                    )
                )
                .on("error", async (err: any) => {
                    this.logger.error(`Copying data error (${this.name}): ${err.toString()}`);
                    await connection.connectionManager.releaseConnection(client);
                    return reject(err);
                })
                .on("finish", async () => {
                    this.logger.info(`Done copying data (${this.name})`);
                    await connection.connectionManager.releaseConnection(client);
                    return resolve();
                });
            data.pipe(stream);
        });
    }

    public GetOne = async (options: LocationConfigOptions): Promise<number[] | null> => {
        if (options.ltStart < 0 || options.ltEnd < 0) {
            throw new GeneralError(`ltStart and ltEnd should not be below zero`, "RsdTmcOsmMappingRepository", undefined, 500);
        }

        try {
            const where = {
                lt_start: options.ltStart,
                lt_end: options.ltEnd,
            };

            const rsdTmcOsmMappingFromDB = await this.sequelizeModel.findOne({
                raw: true,
                where,
            });

            if (rsdTmcOsmMappingFromDB === null) {
                return null;
            }
            return JSON.parse(rsdTmcOsmMappingFromDB.osm_path);
        } catch (err) {
            throw new GeneralError(`GetOne error: ${err.message}`, "RsdTmcOsmMappingRepository", err, 500);
        }
    };
}
