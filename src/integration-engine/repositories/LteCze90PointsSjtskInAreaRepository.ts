import { RsdTmcOsmMappingTable } from "#sch";
import { LteCze90PointsSjtskInAreaModel } from "#sch/models/LteCze90PointsSjtskInAreaModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { col, fn, where } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

export enum InsideProjectAreaEnum {
    PRAVDA = "pravda",
    NEPRAVDA = "nepravda",
}

@injectable()
export class LteCze90PointsSjtskInAreaRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "LteCzePointsSjtskInAreaRepository",
            {
                outputSequelizeAttributes: LteCze90PointsSjtskInAreaModel.attributeModel,
                pgTableName: "ltecze_points_sjtsk_in_area",
                pgSchema: RsdTmcOsmMappingTable.pgSchema,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator("LteCzePointsSjtskInAreaRepository", {}) // table is manually filled
        );
    }

    public async getFiltrationData(): Promise<Array<{ lcd: number }>> {
        return await this.find({
            attributes: ["lcd"],
            where: where(fn("lower", col("inside_project_area")), InsideProjectAreaEnum.PRAVDA.toString()),
            raw: true,
        });
    }
}
