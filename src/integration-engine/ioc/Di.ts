import { RsdTmcOsmMappingDataStreamFactory } from "#ie/datasources/RsdTmcOsmMappingDataStreamFactory";
import { LteCze90PointsSjtskInAreaRepository } from "#ie/repositories/LteCze90PointsSjtskInAreaRepository";
import { RsdTmcOsmMappingRepository } from "#ie/repositories/RsdTmcOsmMappingRepository";
import { IConfiguration, IProtocolStrategy } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer, instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";

//#region Initialization
const TrafficCommonContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasources
TrafficCommonContainer.register<IProtocolStrategy>(ModuleContainerToken.RsdTmcOsmMappingDataSourceStrategy, {
    useFactory: instanceCachingFactory<IProtocolStrategy>(() => {
        const config: IConfiguration = IntegrationEngineContainer.resolve(ContainerToken.Config);
        return new RsdTmcOsmMappingDataStreamFactory().getProtocolStrategyStreamed(
            config.datasources.TrafficCommon.RsdTmcOsmMapping.url
        );
    }),
});
//#endregion

//#region Transformations

//#endregion

//#region Helpers

//#endregion

//#region Repositories
TrafficCommonContainer.registerSingleton<RsdTmcOsmMappingRepository>(
    ModuleContainerToken.RsdTmcOsmMappingRepository,
    RsdTmcOsmMappingRepository
).register<LteCze90PointsSjtskInAreaRepository>(
    ModuleContainerToken.LteCze90PointsSjtskInAreaRepository,
    LteCze90PointsSjtskInAreaRepository
);
//#endregion

//#region Services

//#endregion

export { TrafficCommonContainer };
