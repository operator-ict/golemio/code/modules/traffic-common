const ModuleContainerToken = {
    RsdTmcOsmMappingDataSourceStrategy: Symbol(),
    RsdTmcOsmMappingRepository: Symbol(),
    LteCze90PointsSjtskInAreaRepository: Symbol(),
};

export { ModuleContainerToken };
