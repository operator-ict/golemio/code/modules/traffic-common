import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { RsdTmcOsmMappingModel, RsdTmcOsmMappingTable } from "#sch";

export interface LocationConfigOptions {
    ltStart: number;
    ltEnd: number;
}

export class RsdTmcOsmMappingRepository extends SequelizeModel {
    public constructor() {
        super("RsdTmcOsmMappingRepository", RsdTmcOsmMappingTable.pgTableName, RsdTmcOsmMappingModel.attributeModel, {
            schema: RsdTmcOsmMappingTable.pgSchema,
            timestamps: false,
        });
        this.sequelizeModel.removeAttribute("id");
    }

    public GetOne = async (options: LocationConfigOptions): Promise<number[] | null> => {
        if (options.ltStart < 0 || options.ltEnd < 0) {
            throw new GeneralError(`ltStart and ltEnd should not be below zero`, "RsdTmcOsmMappingRepository", undefined, 500);
        }

        try {
            const where = {
                lt_start: options.ltStart,
                lt_end: options.ltEnd,
            };

            const rsdTmcOsmMappingFromDB = await this.sequelizeModel.findOne({
                raw: true,
                where,
            });

            if (rsdTmcOsmMappingFromDB === null) {
                return null;
            }
            return JSON.parse(rsdTmcOsmMappingFromDB.osm_path);
        } catch (err) {
            throw new GeneralError(`GetOne error: ${err.message}`, "RsdTmcOsmMappingRepository", err, 500);
        }
    };

    public GetAll = async (): Promise<object | null> => {
        throw new GeneralError("Not implemented", "RsdTmcOsmMappingRepository");
    };
}
