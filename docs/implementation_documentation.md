# Implementační dokumentace modulu *traffic-common*

## Záměr

Modul spravuje dopravní data využívaná ostatními moduly (konkrétně NDIC a FCD)


## Vstupní data

**RsdTmcOsmMapping**:

-   vstup je CSV soubor z OSM-TMC, poskytuje OICT

### Data aktivně stahujeme

#### *open data IPR*

- zdroj dat
  - CSV soubor, URL je v configu datasources
- formát dat
  - protokol: http
  - datový typ: csv
  - [struktura](../src/schema-definitions/datasources/RsdTmcOsmMapping.ts)
  - příklad [vstupních dat](../test/integration-engine/data/rsd_tmc_osm_test.csv)
- frekvence stahování
  - cron definice:
    - cron.dataplatform.RsdTmcOsmMapping.RsdTmsOsmMappingRefresh
      - rabin `30 4 * * SUN`
      - prod `30 4 * * SUN`
- název rabbitmq fronty
  - dataplatform.RsdTmcOsmMapping.RsdTmsOsmMappingRefresh

### *RsdTmcOsmMappingWorker*

#### *task: RefreshRsdTmcOsmMappingDataTask*

- vstupní rabbitmq fronta
  - název: dataplatform.rsdtmcosmmapping.RsdTmsOsmMappingRefresh
  - bez parametrů
- datové zdroje
  - RsdTmcOsmMapping CSV
  - data se streamují napřímo do databáze
- data modely
  - RsdTmcOsmMappingModel -> (schéma traffic) `rsd_tmc_osm_mapping`

## Uložení dat

- typ databáze
    - PSQL
- tabulka
    - `traffic.rsd_tmc_osm_mapping`
- retence dat
    - task obnoví data při každém běhu

