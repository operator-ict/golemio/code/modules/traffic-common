import { RsdTmcOsmMappingRepository } from "#ie/repositories/RsdTmcOsmMappingRepository";
import { RefreshRsdTmcOsmMappingDataTask } from "#ie/workers/tasks/RefreshRsdTmcOsmMappingDataTask";
import { DataSourceStreamed } from "@golemio/core/dist/integration-engine";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";

describe("RefreshRsdTmcOsmMappingDataTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshRsdTmcOsmMappingDataTask;

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });

        const fakeRepository = {
            replaceWithStreamData: async (data: any) => {},
        } as unknown as RsdTmcOsmMappingRepository;

        task = new RefreshRsdTmcOsmMappingDataTask(
            "test.rsdtmcosmmapping",
            {
                getRawData: async () => {},
            } as HTTPRequestProtocolStrategyStreamed,
            fakeRepository
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call method correctly", async () => {
        sandbox.stub(task["streamedStrategy"], "getRawData").resolves({} as DataSourceStreamed);
        sandbox.stub(task["repository"], "replaceWithStreamData").resolves();
        await task["execute"]();
        sandbox.assert.calledOnce(task["streamedStrategy"].getRawData as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].replaceWithStreamData as SinonSpy);
        sandbox.assert.callOrder(
            task["streamedStrategy"].getRawData as SinonSpy,
            task["repository"].replaceWithStreamData as SinonSpy
        );
    });
});
