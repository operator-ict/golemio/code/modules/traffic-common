import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { RsdTmcOsmMappingWorker } from "#ie/workers/RsdTmcOsmMappingWorker";
import { TrafficCommonContainer } from "#ie/ioc/Di";

describe("RsdTmcOsmMappingWorker", () => {
    let sandbox: SinonSandbox;
    let worker: RsdTmcOsmMappingWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(TrafficCommonContainer, "resolve").callsFake(() => {});
        worker = new RsdTmcOsmMappingWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".rsdtmcosmmapping");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("RsdTmcOsmMapping");
            expect(result.queues.length).to.equal(1);
        });
    });

    describe("registerTask", () => {
        it("should have one task registered", () => {
            expect(worker["queues"].length).to.equal(1);
            expect(worker["queues"][0].name).to.equal("RsdTmsOsmMappingRefresh");
            expect(worker["queues"][0].options.messageTtl).to.equal(59 * 60 * 1000);
        });
    });
});
