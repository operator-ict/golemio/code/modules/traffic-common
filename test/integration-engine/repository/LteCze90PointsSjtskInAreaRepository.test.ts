import { TrafficCommonContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { LteCze90PointsSjtskInAreaRepository } from "#ie/repositories/LteCze90PointsSjtskInAreaRepository";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { createSandbox, SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("LteCze90PointsSjtskInAreaRepository", () => {
    let sandbox: SinonSandbox;

    before(() => {
        TrafficCommonContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresConnector).connect();
    });

    beforeEach(() => {
        sandbox = createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(() => {
        TrafficCommonContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresConnector).disconnect();
    });

    it("filtration data loading", async () => {
        const repository = TrafficCommonContainer.resolve<LteCze90PointsSjtskInAreaRepository>(
            ModuleContainerToken.LteCze90PointsSjtskInAreaRepository
        );
        const result = await repository.getFiltrationData();
        expect(result).to.deep.equal([{ lcd: 22579 }]);
    });
});
