import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { RsdTmcOsmMappingRepository } from "#og/repositories/RsdTmcOsmMappingRepository";

chai.use(chaiAsPromised);

describe("RsdTmcOsmMappingRepository", () => {
    let rsdTmcOsmMappingRepository: RsdTmcOsmMappingRepository;

    before(() => {
        OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase).connect();
        rsdTmcOsmMappingRepository = new RsdTmcOsmMappingRepository();
    });

    it("should instantiate", () => {
        expect(rsdTmcOsmMappingRepository).not.to.be.undefined;
    });

    describe("GetOne", async () => {
        it("should return correct subset of items", async () => {
            const result = await rsdTmcOsmMappingRepository.GetOne({ ltStart: 25631, ltEnd: 25632 });
            expect(result).to.be.an.instanceOf(Array);
            expect(result).to.eql([156250932, 156250937, 74153579, 976485311]);
        });

        it("throws on invalid input", async () => {
            await expect(rsdTmcOsmMappingRepository.GetOne({ ltStart: -25631, ltEnd: 25632 })).to.be.rejected;
            await expect(rsdTmcOsmMappingRepository.GetOne({ ltStart: 25631, ltEnd: -222 })).to.be.rejected;
            expect(rsdTmcOsmMappingRepository.GetOne({ ltStart: -25631, ltEnd: -1 })).to.be.rejected;
        });
    });
});
