# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.9] - 2024-09-12

### Added

-   possiblity to load osm mapping form ie ([p0131#168](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/168))

## [1.0.8] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.0.7] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.0.6] - 2024-04-08

### Changed

-   migrate from axios to undici

## [1.0.5] - 2024-02-12

-   No changelog

## [1.0.4] - 2024-01-29

### Changed

-   minor changes regarding lookup table `LteCzePointsSjtskInArea` ([p0131#161](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/161))

## [1.0.3] - 2024-01-17

### Added

-	add new table ltecze_points_sjtsk_in_area

## [1.0.2] - 2023-12-13

### Added

-	Function get_route - vrací geom pro route_lcd a versi lokalizačních tabulek

## [1.0.1] - 2023-10-25

### Fixed

-   Data outages of the table `rsd_tmc_osm_mapping`, occurring when old data is being replaced with new data ([p0131#149](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/149))

## [1.0.0] - 2023-10-18

### Changed

-   Migration of RSD tables from the public schema([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [0.1.3] - 2023-06-26

### Changed

-   Change tests after refactoring connectors

## [0.1.2] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [0.1.1] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [0.1.0] - 2023-05-29

### Added

-   Rsd Tmc Osm Mapping updates in database ([ipt#123](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/123))
-   Filtration table for Fcd and ndic data ([ipt#129](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/129))

