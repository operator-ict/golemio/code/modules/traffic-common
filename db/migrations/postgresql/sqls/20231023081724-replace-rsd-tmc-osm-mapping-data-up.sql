CREATE PROCEDURE create_tmp_table(IN tableName Text, OUT tableNameTmp Text)
    LANGUAGE plpgsql
    SET search_path FROM CURRENT
AS $PROCEDURE$
    BEGIN
        SELECT concat(tableName, '_tmp') INTO tableNameTmp;
        EXECUTE format('DROP TABLE IF EXISTS %I;', tableNameTmp);
        EXECUTE format('CREATE TABLE %I (LIKE %I INCLUDING ALL);', tableNameTmp, tableName);
    END;
$PROCEDURE$;

CREATE PROCEDURE replace_with_tmp_table(IN tableName Text)
    LANGUAGE plpgsql
    SET search_path FROM CURRENT
AS $PROCEDURE$
    DECLARE
        tableNameOld Text;
        tableNameTmp Text;
    BEGIN
        SELECT concat(tableName, '_old') INTO tableNameOld;
        SELECT concat(tableName, '_tmp') INTO tableNameTmp;

        EXECUTE format('LOCK %I IN EXCLUSIVE MODE', tableName);
        EXECUTE format('ALTER TABLE %I RENAME TO %I', tableName, tableNameOld);
        EXECUTE format('ALTER TABLE %I RENAME TO %I', tableNameTmp, tableName);
        EXECUTE format('DROP TABLE %I', tableNameOld);
    END;
$PROCEDURE$;
