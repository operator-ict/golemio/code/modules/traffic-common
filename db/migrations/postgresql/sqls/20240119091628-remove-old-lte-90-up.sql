DROP TABLE ltecze90_points_sjtsk_in_area;
COMMENT ON TABLE ltecze_points_sjtsk_in_area IS 'Tabulka definuje zájmový geo-region pro následnou filtraci dat ze zdrojů FCD a NDIC pro projekt IPT. Vlastní filtr se využívá  před uložením do tabulek fcd_traff_params_regions, ndic_traffic_restrictions_regions.';
