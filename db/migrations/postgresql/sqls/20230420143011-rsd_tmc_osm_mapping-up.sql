CREATE TABLE if not exists rsd_tmc_osm_mapping (
    lt_start int8 NULL,
    lt_end int8 NULL,
    osm_path text NULL
);
CREATE INDEX if not exists rsd_tmc_osm_mapping_idx ON rsd_tmc_osm_mapping USING btree (lt_start, lt_end);
