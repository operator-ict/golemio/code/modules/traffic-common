
CREATE TABLE IF NOT EXISTS rsd_tmc_admins
(
    cid integer,
    tabcd integer,
    lcd integer NOT NULL,
    class character varying(1024) COLLATE pg_catalog."default",
    tcd integer,
    stcd integer,
    firstname character varying(1024) COLLATE pg_catalog."default",
    area_ref integer,
    area_name character varying(1024) COLLATE pg_catalog."default",
    version_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT rsd_tmc_admins_pkey PRIMARY KEY (lcd, version_id)
);

CREATE TABLE IF NOT EXISTS rsd_tmc_points
(
    cid integer,
    "table" integer,
    lcd integer NOT NULL,
    class character varying(1024) COLLATE pg_catalog."default",
    tcd integer,
    stcd integer,
    jnumber character varying(50) COLLATE pg_catalog."default",
    roadnumber character varying(1024) COLLATE pg_catalog."default",
    roadname character varying(1024) COLLATE pg_catalog."default",
    firstname character varying(1024) COLLATE pg_catalog."default",
    secondname character varying(1024) COLLATE pg_catalog."default",
    area_ref integer,
    area_name character varying(1024) COLLATE pg_catalog."default",
    roa_lcd integer,
    seg_lcd integer,
    roa_type character varying(1024) COLLATE pg_catalog."default",
    inpos integer,
    outpos integer,
    inneg integer,
    outneg integer,
    presentpos integer,
    presentneg integer,
    interrupt integer,
    urban integer,
    int_lcd character varying(1024) COLLATE pg_catalog."default",
    neg_off integer,
    pos_off integer,
    wgs84_x numeric,
    wgs84_y numeric,
    sjtsk_x numeric,
    sjtsk_y numeric,
    isolated integer,
    version_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    wgs84_point geography(Point,4326),
    CONSTRAINT rsd_tmc_points_pkey PRIMARY KEY (lcd, version_id)
);

CREATE TABLE IF NOT EXISTS rsd_tmc_roads
(
    cid integer,
    tabcd integer,
    lcd integer NOT NULL,
    class character varying(1024) COLLATE pg_catalog."default",
    tcd integer,
    stcd integer,
    roadnumber character varying(1024) COLLATE pg_catalog."default",
    roadname character varying(1024) COLLATE pg_catalog."default",
    firstname character varying(1024) COLLATE pg_catalog."default",
    secondname character varying(1024) COLLATE pg_catalog."default",
    area_ref integer,
    area_name character varying(1024) COLLATE pg_catalog."default",
    version_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT rsd_tmc_roads_pkey PRIMARY KEY (lcd, version_id)
);

CREATE TABLE IF NOT EXISTS rsd_tmc_segments
(
    cid integer,
    tabcd integer,
    lcd integer NOT NULL,
    class character varying(1024) COLLATE pg_catalog."default",
    tcd integer,
    stcd integer,
    roadnumber character varying(1024) COLLATE pg_catalog."default",
    roadname character varying(1024) COLLATE pg_catalog."default",
    firstname character varying(1024) COLLATE pg_catalog."default",
    secondname character varying(1024) COLLATE pg_catalog."default",
    area_ref integer,
    area_name character varying(1024) COLLATE pg_catalog."default",
    roa_lcd integer,
    roa_type character varying(1024) COLLATE pg_catalog."default",
    neg_off integer,
    pos_off integer,
    version_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT rsd_tmc_segments_pkey PRIMARY KEY (lcd, version_id)
);