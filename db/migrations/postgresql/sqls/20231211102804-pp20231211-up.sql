CREATE OR REPLACE FUNCTION get_route(p_route_lcd integer, p_version character varying)
 RETURNS geometry
 LANGUAGE plpgsql
 SET search_path from CURRENT
 /* vrací geom pro route_lcd a versi lokalizačních tabulek */
AS $function$
	declare 
		temprow rsd_tmc_points%rowtype;
		x_id_start integer;
		x_id_next integer;
		x_lat real;
		x_long real;
		x_lat_next real;
		x_long_next real;
		x_final geometry;
		x_return geometry;
		x_geom_txt varchar(5000);
	begin

	FOR temprow IN
        SELECT * FROM rsd_tmc_points where roa_lcd = p_route_lcd and version_id = p_version and neg_off = 0
    loop
	    x_id_start = temprow.lcd;
	    x_lat = temprow.wgs84_x;
	    x_long = temprow.wgs84_y;
	   	x_id_next = temprow.pos_off;
		
	   	select wgs84_x, wgs84_y,pos_off  
		into x_lat_next,x_long_next,x_id_next
		from rsd_tmc_points  
		where lcd = x_id_start
			and version_id = p_version;
			
		x_geom_txt = 'linestring ('||x_lat::varchar(150)||' '||x_long::varchar(150)||','||x_lat_next::varchar(150) || ' ' || x_long_next::varchar(150)||')';
		x_final = ST_GEOMETRYFROMTEXT(x_geom_txt,4326);
	    while x_id_next <> 0 loop
		    x_id_start = x_id_next;
			
			select wgs84_x, wgs84_y,pos_off  
			into x_lat_next,x_long_next,x_id_next
			from rsd_tmc_points  
			where lcd = x_id_start
				and version_id = p_version;
			
	  		x_final = ST_AddPoint(x_final, ST_SetSRID(ST_MakePoint(x_lat_next, x_long_next),4326));	    	
	    end loop;
		x_final = ST_AddPoint(x_final, ST_SetSRID(ST_MakePoint(x_lat_next, x_long_next),4326));
	
		x_return = ST_collect(x_return, x_final);
    END LOOP;
		if 	ST_GeometryType(x_return) = 'ST_GeometryCollection' then	   
			return ST_CollectionExtract(x_return, 2);
		else	
			return ST_Multi(x_return);
		end if;
	
	END;
$function$
;

COMMENT ON FUNCTION get_route IS 'Funkce vrací geometrický objekt pro routu lcd a zvolenou verzi';